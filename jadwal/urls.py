from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('create', views.create_jadwal),
    path('', views.jadwal, name='jadwal'),
    path('delete', views.delete)
]
