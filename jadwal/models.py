from django.db import models
import datetime
from django.utils import timezone
from django.core.validators import MinLengthValidator

# Create your models here.
class Jadwal(models.Model):
    name = models.CharField(validators=[MinLengthValidator(3)],max_length=50)
    desc = models.CharField(null=True, max_length=100)
    category = models.CharField(validators=[MinLengthValidator(3)], max_length=30)
    location = models.CharField(validators=[MinLengthValidator(3)], max_length=30)
    date = models.DateField(default=timezone.now)
    time = models.TimeField(default=timezone.now)

    def __str__(self):
        return self.name
